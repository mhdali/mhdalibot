package config

type Config struct {
	BOT bot `toml:"bot"`
}

type bot struct {
	ApiKey     string `toml:"api_key"`
	ListenPort string `toml:"port"`
	ListenHost string `toml:"host"`
	WebHook    string `toml:"webhook"`
}
