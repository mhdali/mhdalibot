package main

import (
	"github.com/BurntSushi/toml"
	"github.com/gocolly/colly"
	"gitlab.com/mhdali/mhdalibot/config"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"net/http"
	s "strings"
)

func main() {
	var conf config.Config

	_, err := toml.DecodeFile("config.toml", &conf)
	if err != nil {
		log.Fatal(err)
	}

	bot, err := tgbotapi.NewBotAPI(conf.BOT.ApiKey)
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// TODO: Configure webhook with TLS
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(conf.BOT.WebHook + bot.Token))
	if err != nil {
		log.Fatal(err)
	}
	info, err := bot.GetWebhookInfo()
	if err != nil {
		log.Fatal(err)
	}
	if info.LastErrorDate != 0 {
		log.Printf("[Telegram callback failed]%s", info.LastErrorMessage)
	}
	updates := bot.ListenForWebhook("/" + bot.Token)
	go http.ListenAndServe(conf.BOT.ListenHost+":"+conf.BOT.ListenPort, nil)

	for update := range updates {
		err := processUpdate(update, bot)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func processUpdate(update tgbotapi.Update, bot *tgbotapi.BotAPI) error {
	log.Printf("%+v\n", update)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)

	chatId := update.Message.Chat.ID
	switch update.Message.Text {
	case "/start":
		bot.Send(tgbotapi.NewMessage(chatId, "Welcome!"))
	case "/gold":
		bot.Send(tgbotapi.NewMessage(chatId, checkGoldPrice()))
	default:
		bot.Send(msg)
	}
	return nil
}

func checkGoldPrice() string {
	c := colly.NewCollector()

	var msg string
	c.OnHTML("#productId2461_quantity1_", func(e *colly.HTMLElement) {
		msg += "<20: " + s.Trim(s.TrimSpace(e.Text), "SGD ") + "\n"
	})

	c.OnHTML("#productId2461_quantity20_", func(e *colly.HTMLElement) {
		msg += ">20: " + s.TrimSpace(e.Text) + "\n"
	})

	// TODO: Figure out a way to parse the second table
	//	c.OnHTML("tr", func(e *colly.HTMLElement) {
	//		switch e.ChildText("td:first-child") {
	//		case "Price/Gram":
	//			log.Printf("Price/Gram: " + e.ChildText("td:nth-child(2)"))
	//		case "Price/Troy Ounce":
	//			log.Printf("Price/Troy Ounce: " + e.ChildText("td:nth-child(2)"))
	//		case "Price Premium":
	//			log.Printf("Price Premium: " + e.ChildText("td:nth-child(2)"))
	//		case "Spread":
	//			log.Printf("Spread: " + e.ChildText("td:nth-child(2)"))
	//		}
	//	})

	c.OnError(func(_ *colly.Response, err error) {
		log.Fatal(err)
	})

	c.Visit("https://www.bullionstar.com/buy/product/gold-coin-maple-leaf-1oz-2018")

	return msg
}
